Student Name: Peter Major
Student Number: 16375246
Supervisor: Rem Collier

Project Name: Distribution Center Manager for Optimising Employee Wellbeing

This project aimed to create a framework, agent based Distribution Center management system
and tools that would be needed to flesh the system out into a usable service.

It was built using a Micro Service Architecture thus is broken into several sub projects:
    /DCManagmentSystem
        /AdvocateAgents: Astra agent program responsible for monitoring pickers
        /PickListGenerator: Java Springboot program for storing orders and generating pick lists
        /TaskManager: Astra agent program for storing and assigning tasks to pickers
    /CDSimulator
        /OrderGenerator: Java Springboot program that generates orders of items
        /PickerAgents: Astra agent program that act as the brain to pickers
        /PlacementPolicy: Java Springboot program that assigns locations to ordered items
        /Simulator: java Springboot program that is a hub for the simulation of a DC
Note: DC = Distribution Center

Requirements:
    - Maven
    - Docker
    - Docker Compose

To run:

* If on linux:
    there is a build script called 'docker-build.sh'. Running this script will build and package
    all of the relevant projects into docker containers
* If on other OS:
    Please go to the following folders and use the given commands
    - DCManagmentSystem/TaskManager 
        mvn package
        docker build . --tag manager-tasks
    - DCManagmentSystem/AdvocateAgents 
        mvn package
        docker build . --tag manager-advocates
    - DCSimulator/PickerAgents  
        mvn package
        docker build . --tag picker-agent
    - DCSimulator/Simulator  
        mvn package
        docker build . --tag simulator

* then in the project root directory run
    docker-compose up

