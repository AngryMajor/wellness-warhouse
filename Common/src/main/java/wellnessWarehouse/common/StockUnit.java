package wellnessWarehouse.common;

public class StockUnit{

    private String name;
    private String currentPosition;
    private String aproxamatePosition;

    public StockUnit(){}

    public StockUnit(String name){
        this.name = name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public String getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(String currentPosition) {
        this.currentPosition = currentPosition;
    }

    public String getAproxamatePosition() {
        return aproxamatePosition;
    }

    public void setAproxamatePosition(String aproxamatePosition) {
        this.aproxamatePosition = aproxamatePosition;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof StockUnit){
            StockUnit other = (StockUnit) obj;
            boolean equal = true;
            if(equal==true && name.equals(other.name)==false)
                equal = false;
            return equal;
        }else
            return false;
        
    }

    @Override
    public String toString() {
        return "Unit : " + getName();
    }

}