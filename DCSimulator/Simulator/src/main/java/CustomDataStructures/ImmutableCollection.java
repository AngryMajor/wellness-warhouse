package CustomDataStructures;

import java.util.Collection;
import java.util.Iterator;

public class ImmutableCollection<T> implements Iterable<T>{

    private Collection<T> collection;

    public ImmutableCollection(Collection<T> set){
        this.collection = set;
    }

    public boolean contains(T arg0) {
        return collection.contains(arg0);
    }

    
    public boolean containsAll(Collection<?> arg0) {
        return collection.containsAll(arg0);
    }

    
    public boolean isEmpty() {
        return collection.isEmpty();
    }

    
    public Iterator<T> iterator() {
        return collection.iterator();
    }

    
    public int size() {
        return collection.size();
    }

    
    public Object[] toArray() {
        return collection.toArray();
    }

    public <X> X[] toArray(X[] arg0) {
        return collection.toArray(arg0);
    }

}