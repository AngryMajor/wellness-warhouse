package Simulator.Controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.web.bind.annotation.*;

import Simulator.Model.Entitties.PickerEntity;

@RestController
@RequestMapping("/pickers")
public class PickerController extends EntityController<PickerEntity> {

    @PostConstruct
    public void init() {
        entityRegistry.init(PickerEntity.class);
    }

    @GetMapping("/{name}/fatigue")
    public ResponseEntity<?> getFatigue(@PathVariable("name") String name){
        PickerEntity entity = entityRegistry.getEntity(name);
        if (entity != null){
            Map<String,Object> envelope = new HashMap<>();
            envelope.put("generalFatigue", entity.getGeneralFatigue());
            return new ResponseEntity<>(envelope, HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    
}