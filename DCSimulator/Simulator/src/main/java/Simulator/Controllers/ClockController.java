package Simulator.Controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Simulator.Services.ClockService;

@RestController
@RequestMapping("/clock")
public class ClockController{

    @Autowired
    ClockService clock;

    @RequestMapping("/tick")
    public ResponseEntity<?> tick(){
        clock.tick();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/subscribers")
    public ResponseEntity<?> subscribe(@RequestBody Map<String,String> body){
        clock.addSubscriber(body.get("location"));
        return new ResponseEntity<>(HttpStatus.OK);
    }

}