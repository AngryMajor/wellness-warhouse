package Simulator.Controllers;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonView;

import org.jgrapht.io.ExportException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import CustomDataStructures.ImmutableCollection;
import Simulator.JacksonSettings.View;
import Simulator.Model.Map.Location;
import Simulator.Model.StoredUnits.StoredUnit;
import Simulator.Services.MapBuilderService;
import Simulator.Services.SimulationMapService;
import Simulator.Services.StockRegistry;
import wellnessWarehouse.common.StockUnit;

@RestController
@RequestMapping("/map")
public class MapController {

    @Autowired
    SimulationMapService DcMap;
    @Autowired
    MapBuilderService manBuilder;
    @Autowired
    StockRegistry stockRegistry;

    /**
     * @return standard jGraphT json object maybe with added content such as Entity
     *         locations
     */
    @GetMapping("")
    public ResponseEntity<?> getMap() {
        try {
            return new ResponseEntity<>(manBuilder.getMapJson(),HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * @return load a map preset or maybe able to send in map
     */
    @PutMapping("")
    public ResponseEntity<?> setMap(@RequestBody Map<String,String> body){       
        return null;
    }

    @GetMapping("paths/{source}/{destination}")
    public ResponseEntity<?> getPath(@PathVariable("source") String source, @PathVariable("destination") String destination){
        
        Location[] path = DcMap.getPath(source,destination).toArray(new Location[0]);
        String[] pathOfNames = new String[path.length];
        for(int i = 0; i < path.length; i++)
            pathOfNames[i] = path[i].getName();

        Map<String,Object> envelope = new HashMap<>();
        envelope.put("path",pathOfNames);
        envelope.put("length",pathOfNames.length);

        return new ResponseEntity<>(envelope,HttpStatus.OK);
    }

    @PostMapping("/orders")
    public ResponseEntity<?> newOrder(@RequestBody OrderMessage order){
        System.out.println(order.getOrder().length);
        for(StockUnit unit : order.getOrder()){
            Location unitsPosition = DcMap.getLocation(unit.getCurrentPosition());
            unitsPosition.addStockUnit(unit);
            StoredUnit storedUnit = new StoredUnit(unit, unitsPosition);
            stockRegistry.addStock(storedUnit);
            System.out.println(unit.getName() + ":" + unit.getCurrentPosition());
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/storedItems")
    @JsonView(View.listing.class)
    public ResponseEntity<?> getStoredItems(){
        return new ResponseEntity<>(stockRegistry.getStoredItems().toArray(), HttpStatus.OK);
    }

    public static class OrderMessage{
        StockUnit[] order;

        public StockUnit[] getOrder() {
            return order;
        }

        public void setOrder(StockUnit[] orderedUnits) {
            this.order = orderedUnits;
        }
    }
}