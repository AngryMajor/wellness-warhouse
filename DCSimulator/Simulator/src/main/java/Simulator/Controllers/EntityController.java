package Simulator.Controllers;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.annotation.JsonView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import Simulator.CustomExceptions.*;
import Simulator.JacksonSettings.View;
import Simulator.Model.Entitties.BaseEntity;
import Simulator.Services.ClockService;
import Simulator.Services.SimulationMapService;
import Simulator.Services.EntityRegistryService;

public abstract class EntityController<E extends BaseEntity> {

    @Autowired
    protected EntityRegistryService<E> entityRegistry;
    @Autowired
    protected SimulationMapService dcMap;
    @Autowired
    protected ClockService clock;
    @Autowired
    protected Environment environment;

    @PostConstruct
    public abstract void init();

    @PostMapping("")
    public ResponseEntity<?> create(@RequestBody Map<String, String> params) throws UnknownHostException {
        try {
            entityRegistry.createEntity(params.get("name"), dcMap.getStartingLocation());
            Map<String,String> returnMap = new HashMap<>();
            returnMap.put("Location", "http://" + InetAddress.getLocalHost().getHostName() + ":8080/pickers/" + params.get("name"));
            return new ResponseEntity<>(returnMap, HttpStatus.OK);
        } catch (FailedToCreateEntityException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        } catch (DuplicateEntityException e) {
            return new ResponseEntity<>("entity with that name already exists", HttpStatus.CONFLICT);
        }
    }

    @GetMapping("")
    @JsonView(View.listing.class)
    public ResponseEntity<?> getEntities() {
        return new ResponseEntity<>(entityRegistry.getEntities().toArray(), HttpStatus.OK);
    }

    @GetMapping("/{name}")
    @JsonView(View.Summary.class)
    public ResponseEntity<?> get(@PathVariable("name") String name) {
        E entity = entityRegistry.getEntity(name);
        if (entity != null)
            return new ResponseEntity<>(entity, HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/{name}/position")
    public ResponseEntity<?> getPosition(@PathVariable("name") String name) {
        if (entityRegistry.getEntity(name).getCurrentPosition() != null){
            Pos p = new Pos();
            p.name = entityRegistry.getEntity(name).getCurrentPosition().getName();
            p.adjPoses = entityRegistry.getEntity(name).getCurrentPosition().getNeighboringLocationNames();
            return new ResponseEntity<>(p, HttpStatus.OK);
        }else
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

    }

    @GetMapping("/{name}/action")
    public ResponseEntity<?> getActionList(@PathVariable("name") String name){
        if(entityRegistry.getEntity(name).getCurrentAction() == null)
            return new ResponseEntity<>("{\"name\":\"null\"}", HttpStatus.OK);
        else
            return new ResponseEntity<>("{\"name\":\""+entityRegistry.getEntity(name).getCurrentAction()+"\"}", HttpStatus.OK);
    }

    @PutMapping("/{name}/action")
    public ResponseEntity<?> putAction(@RequestBody Map<String, Object> params, @PathVariable("name") String entityName) {
        try{
            entityRegistry.getEntity(entityName).setCurrentAction(((String)params.get("type")),params);
        } catch(ActionNotFoundException e){
            e.printStackTrace();
            return new ResponseEntity<>("Action not Found: " + params.get("type"),HttpStatus.BAD_REQUEST);
        } catch(MissingParameterException e){
            e.printStackTrace();
            return new ResponseEntity<>("Missing Parameters for: " + params.get("type"),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{name}/action/history")
    public ResponseEntity<?> getActionHistory(@PathVariable("name") String name){
        E entity = entityRegistry.getEntity(name);
        if (entity != null)
            return new ResponseEntity<>(entity.getActionHistory().toArray(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    public class Pos{
        public String name;
        public String[] adjPoses;
    }

}