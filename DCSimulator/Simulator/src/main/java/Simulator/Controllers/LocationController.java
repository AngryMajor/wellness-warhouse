package Simulator.Controllers;

import com.fasterxml.jackson.annotation.JsonView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import Simulator.JacksonSettings.View;
import Simulator.Model.Map.Location;
import Simulator.Services.SimulationMapService;

@RestController
@RequestMapping("map/locations")
public class LocationController{

    @Autowired
    SimulationMapService DcMap;

    /**
     * @return returns list of all location names
     */
    @GetMapping("")
    @JsonView(View.listing.class)
    public ResponseEntity<?> getLocations(){
        return new ResponseEntity<>(DcMap.getLocations().toArray(), HttpStatus.OK);
    }

    /**
     * @param id name of location
     * @return details about location, standard spring boot json
     */
    @GetMapping("/{id}")
    public ResponseEntity<?> getLocation(@PathVariable("id") String id){
        if(DcMap.getLocation(id) != null)
            return new ResponseEntity<Location>(DcMap.getLocation(id), HttpStatus.OK);
        else 
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/{id}/stored_units")
    public ResponseEntity<?> getStoredUnits(@PathVariable String id){
        return new ResponseEntity<>(DcMap.getLocation(id).getStoredUnits().toArray(),HttpStatus.OK);
    }

    /**
     * @return list of location names reachable from given location
     */
    @GetMapping("/{id}/adjacents")
    @JsonView(View.Summary.class)
    public ResponseEntity<?> getAdjacentLocations(@PathVariable("id") String id){
        Location currLocation = DcMap.getLocation(id);
        return new ResponseEntity<>(currLocation.getNeighboringLocationNames(), HttpStatus.OK);
    }



}