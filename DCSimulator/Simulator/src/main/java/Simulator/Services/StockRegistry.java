package Simulator.Services;

import CustomDataStructures.ImmutableCollection;
import Simulator.Model.StoredUnits.StoredUnit;

public interface StockRegistry{
    public void addStock(StoredUnit unit);
    public ImmutableCollection<StoredUnit> getStoredItems();
}