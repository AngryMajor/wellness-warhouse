package Simulator.Services;

import java.io.IOException;

public interface MapBuilderService {
    public String getMapJson() throws IOException ;
}