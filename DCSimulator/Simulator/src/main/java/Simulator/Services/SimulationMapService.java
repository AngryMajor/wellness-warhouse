package Simulator.Services;

import java.io.Writer;

import org.jgrapht.io.ExportException;

import CustomDataStructures.ImmutableCollection;
import Simulator.Model.Map.*;

public interface SimulationMapService {

    public void exportMap(Writer output) throws ExportException;

    public Location getLocation(String id);
    public ImmutableCollection<Location> getLocations();
    public ImmutableCollection<Location> getNeighboringLocationsOf(Location vertex);
    public Location getStartingLocation();
    public ImmutableCollection<Location> getPath(String source, String destination);
}