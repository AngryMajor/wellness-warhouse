package Simulator.Services;

import CustomDataStructures.ImmutableCollection;
import Simulator.CustomExceptions.*;
import Simulator.Model.Entitties.BaseEntity;
import Simulator.Model.Map.Location;

public interface EntityRegistryService <T extends BaseEntity>{
    public void init(Class<T> builder);
    public void createEntity(String name, Location StartingLocation) 
        throws FailedToCreateEntityException, DuplicateEntityException;
    public T getEntity(String name);
    public ImmutableCollection<T> getEntities();
}