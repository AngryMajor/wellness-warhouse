package Simulator.Services;

import CustomDataStructures.ImmutableCollection;

public interface ClockService {

    public void tick();
    public boolean isStarted();
    public void start();
    public void stop();
    public void addSubscriber(String address);

}