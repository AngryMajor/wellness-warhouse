package Simulator.Model.Entitties;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonView;

import Simulator.JacksonSettings.View;
import Simulator.Model.Clock.Clockable;
import Simulator.Model.Map.Location;

public class MoveableEntity extends BaseEntity {

    private double movementSpeed = 1;

    public MoveableEntity(String name, Location startingLocation, Clockable clock){
        super(name, startingLocation, clock);
    }

    public double getMovementSpeed(){
        return movementSpeed;
    }

    protected void setMovementSpeed(double value){
        movementSpeed = value;
    }

    ////////////////////////
    //Actions
    ////////////////////////

    public class Move extends Action{

        protected Move(Map<String,Object> params){
            super(params);
            setWorkLeft(1);
        } 

        @Override
        public boolean paramsOk(Map<String, Object> params) {
            return hasParamOfType("destination", String.class)
                && currLocation.isNeighborTo((String)params.get("destination"));
        }

        @Override
        public void tick() {
            reduceWorkLeft(getMovementSpeed());
        }

        @Override
        public void finish() {
            setCurrentPosition(currLocation.getNeighbor((String)params.get("destination")));
        }

    }

}