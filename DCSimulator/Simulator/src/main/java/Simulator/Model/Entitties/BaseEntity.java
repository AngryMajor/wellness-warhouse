package Simulator.Model.Entitties;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Stack;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import CustomDataStructures.ImmutableCollection;
import Simulator.CustomExceptions.ActionNotFoundException;
import Simulator.CustomExceptions.MissingParameterException;
import Simulator.JacksonSettings.View;
import Simulator.Model.Clock.Clockable;
import Simulator.Model.Clock.Tickable;
import Simulator.Model.Map.Location;

public class BaseEntity implements Tickable {

    @JsonView(View.listing.class)
    Location currLocation;
    @JsonView(View.listing.class)
    String name;
    
    @JsonIgnore
    Clockable clock;

    @JsonIgnore
    Map<String, Constructor<Action>> actionMap = new HashMap<>();
    @JsonView(View.listing.class)
    Action currentAction;
    int startTickOfCurrentAction;

    Deque<ActionLogEntry> actionHistory = new LinkedList<>();

    public BaseEntity(String name, Location StartingLocation, Clockable clock) {
        setCurrentPosition(StartingLocation);
        setName(name);
        populateActionMap();
        this.clock = clock;
        if (clock != null)
            clock.register(this);
    }

    @SuppressWarnings("unchecked")
    protected void populateActionMap() {
        Class<?> clazz = this.getClass();

        while (BaseEntity.class.isAssignableFrom(clazz)) {
            for (Class<?> c : clazz.getDeclaredClasses()) {
                if (Action.class.isAssignableFrom(c) && c != Action.class
                        && actionMap.containsKey(c.getSimpleName()) == false) {
                    try {
                        actionMap.put(c.getSimpleName(), (Constructor<Action>) c
                                .getDeclaredConstructor(clazz, Map.class ));
                    } catch (NoSuchMethodException | SecurityException e) {
                        e.printStackTrace();
                    }
                }
            }
            clazz = clazz.getSuperclass();
        }
    }

    public Location getCurrentPosition() {
        return currLocation;
    }

    void setCurrentPosition(Location target) {
        currLocation = target;
    }

    public String getName() {
        return name;
    }

    void setName(String name) {
        BaseEntity.this.name = name;
    }

    public void setCurrentAction(String actionName, Map<String, Object> params)
            throws ActionNotFoundException, MissingParameterException {
        if (actionMap.containsKey(actionName) == false)
            throw new ActionNotFoundException();

        try {
            currentAction = actionMap.get(actionName).newInstance(this, params);
            startTickOfCurrentAction = clock.getTickNumber();
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
            e.printStackTrace();
        }

        if(currentAction.paramsOk(params) == false){
            currentAction = null;
            throw new MissingParameterException();
        }

        if(clock != null)
            clock.ready(this);
    }

    public String getCurrentAction(){
        if(currentAction == null)
            return null;
        else
            return currentAction.toString();
    }

    private void tickCurrentAction() {
        if (currentAction != null){
            currentAction.tick();
                
            System.out.println("performing action: " + currentAction.getClass().getSimpleName());

            if(currentAction.getWorkLeft() <= 0){
                currentAction.finish();
                actionHistory.push(new ActionLogEntry(currentAction.getName(), 
                    currentAction.getParams(), startTickOfCurrentAction, clock.getTickNumber() ));
                currentAction = null;
                clock.unready(this);
            }
        }
    }

    @Override
    public void tick() {
        tickCurrentAction();
    }

    public ImmutableCollection<ActionLogEntry> getActionHistory(){
        return new ImmutableCollection<>(actionHistory);
    }

    /////////////////
    //Actions
    /////////////////

    public abstract class Action{
        protected Map<String,Object> params;
        private double workLeft;

        protected Action(Map<String,Object> params){
            setWorkLeft(1);
            this.params = params;
        }
        
        public abstract boolean paramsOk(Map<String,Object> params);
        //public abstract boolean canPerform();
        public abstract void tick();
        public abstract void finish();

        protected double getWorkLeft(){
            return workLeft;
        }

        protected void setWorkLeft(double value){
            workLeft = value;
        }

        protected void reduceWorkLeft(double value){
            workLeft -= value;
        }

        protected boolean hasParamOfType(String name, Class<?> type){
            return params.containsKey(name) && (type.isInstance(params.get(name)));
        }

        public String getName(){
            return this.getClass().getSimpleName();
        }

        public Map<String,Object> getParams(){
            return params;
        }

        @Override
        public String toString() {
            return this.getClass().getSimpleName();
        }
    }

    public class Test extends Action{

        protected Test(Map<String, Object> params) {
            super(params);
            setWorkLeft(1);
        }

        @Override
        public boolean paramsOk(Map<String, Object> params) {
            return true;
        }

        @Override
        public void tick() {
            reduceWorkLeft(1);
        }

        @Override
        public void finish() {}
        
    }

}