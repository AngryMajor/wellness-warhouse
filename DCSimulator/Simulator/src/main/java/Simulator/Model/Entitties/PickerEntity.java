package Simulator.Model.Entitties;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonView;

import Simulator.JacksonSettings.View;
import Simulator.Model.Clock.Clockable;
import Simulator.Model.Map.Location;
import wellnessWarehouse.common.StockUnit;

public class PickerEntity extends HumanEntity {

    @JsonView(View.Summary.class)
    Map<String,StockUnit> heldItems = new HashMap<>();

    public PickerEntity(String name, Location startingLocation, Clockable clock){
        super(name, startingLocation, clock);
    }

    public class PickUp extends Action{

        String targetItem;        

        protected PickUp(Map<String, Object> params) {
            super(params);
            setWorkLeft(1);
            targetItem = (String)params.get("target");
        }

        @Override
        public boolean paramsOk(Map<String, Object> params) {
            if(params.containsKey("target") == false)
                return false;
            else{
                String unitToPick = (String)params.get("target");
                return getCurrentPosition().hasStockUnit(unitToPick);
            }
        }

        @Override
        public void tick() {
            reduceWorkLeft(1);
        }

        @Override
        public void finish() {
            StockUnit su = getCurrentPosition().removeStoredUnit(targetItem);
            heldItems.put(su.getName(),su);
            System.out.println("picked up: " + targetItem + " is carrying: " + heldItems);
        }
        
    }

    public class PutDown extends Action{

        String targetItem;

        protected PutDown(Map<String, Object> params) {
            super(params);
            setWorkLeft(1);
            targetItem = (String) params.get("target");
        }

        @Override
        public boolean paramsOk(Map<String, Object> params) {
            return params.containsKey("target") && (params.get("target").equals("") == false);
        }

        @Override
        public void tick() {
            reduceWorkLeft(1);
        }

        @Override
        public void finish() {
            if(targetItem.equals("InfiniteItem"))
                heldItems.clear();
            else{
                StockUnit su = heldItems.get(targetItem);
                getCurrentPosition().addStockUnit(su);
            }
        }
    }

    public class PickerTest extends Action{

        protected PickerTest( Map<String, Object> params) {
            super(params);
        }

        @Override
        public boolean paramsOk(Map<String, Object> params) {
            return true;
        }

        @Override
        public void tick() {

        }

        @Override
        public void finish() {

        }
        
    }

}