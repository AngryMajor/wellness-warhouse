package Simulator.Model.Entitties;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonView;

import Simulator.JacksonSettings.View;
import Simulator.Model.Clock.Clockable;
import Simulator.Model.Map.Location;

public class HumanEntity extends MoveableEntity {
    protected final double Base_MOVEMENT_SPEED = 5;
    protected final double REST_RECUERY_RATE = 0.1;

    @JsonView(View.Summary.class)
    private double generalFatigueLevel = 0;

    public HumanEntity(String name, Location startingLocation, Clockable clock){
        super(name, startingLocation, clock);
        setMovementSpeed(Base_MOVEMENT_SPEED);
    }

    @Override
    public void tick() {
        super.tick();

        double distanceWalked = calculateDistanceWalked();
        int actionReps = 0;
        double environmentPenalty = getCurrentPosition().getLocationFatiguePenalty();
        double toolPenalty = 0.0;
        generalFatigueLevel += 
            FatigueAccumulationFunction(distanceWalked, actionReps, environmentPenalty, toolPenalty);
    
        System.out.println("Fatigue Level now: " + getGeneralFatigue() + " movment speed: " + getMovementSpeed());
    }

    public double getGeneralFatigue(){
        return generalFatigueLevel;
    }

    @Override
    public double getMovementSpeed() {
        setMovementSpeed(Base_MOVEMENT_SPEED/(1+(getGeneralFatigue())/10));
        return super.getMovementSpeed();
    }

    protected double calculateDistanceWalked(){

        if(currentAction != null && currentAction.getClass().equals(Move.class)){
            return getMovementSpeed();
        }else
            return 0.0;
    }

    protected double FatigueAccumulationFunction(double distanceWalked, int actionReps,
    double environmentPenalty, double toolPenalty){
        return ((1 + distanceWalked) * environmentPenalty)/10;
     }

    /////////////////
    //Actions
    /////////////////

    public class Rest extends BaseEntity.Action{

        protected Rest(Map<String, Object> params) {
            super(params);
        }

        @Override
        public boolean paramsOk(Map<String, Object> params) {
            return true;
        }

        @Override
        public void tick() {
            //Do not reduce work left as this is an infinite action that must be interrupted
            generalFatigueLevel -= 0.2;
        }

        @Override
        public void finish() {
            //infinite action, never finishes.
        }
    }
}