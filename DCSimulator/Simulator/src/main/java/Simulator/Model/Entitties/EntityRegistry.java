package Simulator.Model.Entitties;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import CustomDataStructures.ImmutableCollection;
import Simulator.Model.Clock.Clock;
import Simulator.Model.Clock.Clockable;
import Simulator.CustomExceptions.*;
import Simulator.Model.Entitties.BaseEntity;
import Simulator.Model.Map.Location;
import Simulator.Services.EntityRegistryService;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EntityRegistry<T extends BaseEntity> implements EntityRegistryService<T> {

    private static Map<String, BaseEntity> entityMap = new ConcurrentHashMap<>();

    private Class<T> entityClass;

    public static void resetAllRegistries() {
        entityMap = new HashMap<>();
    }

    @Override
    public void init(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public void createEntity(String name, Location StartingLocation)
            throws FailedToCreateEntityException, DuplicateEntityException {
        if (entityMap.containsKey(name))
            throw new DuplicateEntityException();
        try {
            entityMap.put(name, entityClass.getConstructor(new Class<?>[] {String.class, Location.class, Clockable.class})
            .newInstance(name, StartingLocation,Clock.getInstance()));
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
                | NoSuchMethodException | SecurityException e) {
            e.printStackTrace();
        }

    }

    @Override
    @SuppressWarnings("unchecked")
    public T getEntity(String name) {
        BaseEntity entity = entityMap.get(name);

        if(entity != null && (isParentType(entity)))
            return (T) entity;
      
        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ImmutableCollection<T> getEntities() {
        List<T> entitiesFound = new LinkedList<>();
        for(BaseEntity entity : entityMap.values())
            if(isParentType(entity))
                entitiesFound.add((T)entity);
        return new ImmutableCollection<>(entitiesFound);
    }

    private boolean isParentType(BaseEntity entity){
        return (entityClass.isInstance(entity));
    }

}