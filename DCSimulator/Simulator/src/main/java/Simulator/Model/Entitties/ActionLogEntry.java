package Simulator.Model.Entitties;

import java.util.Map;

public class ActionLogEntry {
    private String name;
    private Map<String,Object> params;
    private int tickCompletedOn;
    private int tickStartedOn;

    public ActionLogEntry(String actionName, Map<String, Object> params,int tickStartedOn, int tickCompletedOn) {
        this.name = actionName;
        this.params = params;
        this.tickCompletedOn = tickCompletedOn;
        this.tickStartedOn = tickStartedOn;
    }

    public String getName() {
        return name;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public int getTickCompletedOn() {
        return tickCompletedOn;
    } 
    
    public int getTickStartedOn() {
        return tickStartedOn;
    } 
}
