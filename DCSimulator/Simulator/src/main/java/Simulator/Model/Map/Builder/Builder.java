package Simulator.Model.Map.Builder;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.io.ImportException;
import org.jgrapht.io.JSONImporter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import Simulator.Model.Map.*;
import Simulator.Services.MapBuilderService;

@Configuration
@ConfigurationProperties(prefix = "map")
public class Builder implements MapBuilderService{

    private String file;
    InputStream configFile;

    public void setFile(String file){
        this.file = file;
    }

    public String getFile(){
        return file;
    }

    @Bean
    public SimulationMap createMap() throws IOException, ImportException{
        SimulationMap map = new SimulationMap();

        Resource r = new ClassPathResource("MapFiles/" + file + ".json");
        configFile = r.getInputStream();

        //configFile = new File(Builder.class.getClassLoader().getResource("MapFiles/" + file + ".json").getFile());
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(configFile);
        

        Reader jgraphtReader = new StringReader(root.get("jgraphtGraph").toString());
        JSONImporter<Location,DefaultEdge> importer = new JSONImporter<>(map.new VertexSupplier(), map.new EdgeSupplier());
        importer.importGraph(map, jgraphtReader);

        return map;
    }

    @Override
    public String getMapJson() throws IOException {
        return configFile.toString();
    }

}