package Simulator.Model.Map;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import org.jgrapht.io.*;

import CustomDataStructures.ImmutableCollection;
import Simulator.JacksonSettings.View;
import Simulator.Model.Entitties.BaseEntity;
import wellnessWarehouse.common.StockUnit;

public class Location {

    @JsonIgnore
    private SimulationMap dcMap;
    @JsonView(View.listing.class)
    private String name;

    @JsonView(View.Summary.class)
    private Map<String,StockUnit> storedUnits = new HashMap<>();

    public Location(){}

    public Location(SimulationMap map) {
        this(map,"defaultLocationName",new HashMap<String,Attribute>());
    }

    public Location(SimulationMap map, String name) {
        this(map,name,new HashMap<String,Attribute>());
    }

    public Location(SimulationMap map, String name, Map<String,Attribute> attributes) {
        this.dcMap = map;
        this.name = name;

        map.Register(name, this);
    }

    @JsonIgnore
    public Location[] getNeighboringLocations() {
        return dcMap.getNeighboringLocationsOf(this).toArray(new Location[0]);
    }

    public String[] getNeighboringLocationNames() {
        List<String> nameList = new LinkedList<>();
        for(Location local : dcMap.getNeighboringLocationsOf(this))
            nameList.add(local.name);
        return nameList.toArray(new String[0]);

    }

    public Boolean isNeighborTo(Location target){
        return dcMap.getNeighboringLocationsOf(this).contains(target);
    }

    public boolean isNeighborTo(String target){
        return isNeighborTo(dcMap.getLocation(target));
    }

    public Location getNeighbor(String target){
        return dcMap.getLocation(target);
    }
    
    public void addStockUnit(StockUnit su){
        if(su.getName().equals("InfiniteItem") == false)
            storedUnits.put(su.getName(),su);
    }

    public ImmutableCollection<StockUnit> getStoredUnits(){
        return new ImmutableCollection<>(storedUnits.values());
    }

    public StockUnit removeStoredUnit(String id){
        if(id.equals("InfiniteItem"))
            return new StockUnit("InfiniteItem");
        else
            return storedUnits.remove(id);
    }

    public boolean hasStockUnit(String id){
        if(id.equals("InfiniteItem"))
            return true;
        else
            return storedUnits.containsKey(id);
    }

	public String getName() {
		return name;
	}

    public double getLocationFatiguePenalty(){
        return 1.0;
    }

    @JsonIgnore
	public java.util.Map<String, Attribute> getAttributes() {
        java.util.Map<String,Attribute> attributeMap = new java.util.HashMap<>();

        attributeMap.put("Type", new DefaultAttribute<String>(this.getClass().getName(), AttributeType.STRING));

		return attributeMap;
	}

    public Map<String, String> getCharacteristics() {
        Map<String,String> characteristics = new HashMap<>();
        characteristics.put("Type",this.getClass().getSimpleName());
        return characteristics;
    }


}