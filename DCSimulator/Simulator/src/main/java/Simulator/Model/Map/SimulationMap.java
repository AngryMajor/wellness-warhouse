package Simulator.Model.Map;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import org.jgrapht.*;
import org.jgrapht.io.*;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultUndirectedGraph;
import org.jgrapht.alg.*;
import org.jgrapht.alg.shortestpath.AStarShortestPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;

import CustomDataStructures.ImmutableCollection;
import Simulator.Services.SimulationMapService;


public class SimulationMap extends DefaultUndirectedGraph<Location, DefaultEdge> 
    implements SimulationMapService {
    private static final long serialVersionUID = -8739504205081360128L;

    VertexProvider<Location> vertexProvider = new VertexSupplier();
    Map<String,Location> locationMap = new HashMap<>();
    Location startingLocation = null;

    public SimulationMap() {
        super(DefaultEdge.class);
    }

    public ImmutableCollection<Location> getNeighboringLocationsOf(Location vertex) {
        return new ImmutableCollection<Location>(Graphs.neighborSetOf(this, vertex));
    }

    public Location getStartingLocation(){
        return startingLocation;    
    }
    
    public void setStartingLocation(Location startingLocation) {
        this.startingLocation = startingLocation;
    }
    
    public void importMap(Reader input) throws ImportException {
        JSONImporter<Location, DefaultEdge> importer = new JSONImporter<Location, DefaultEdge>(new VertexSupplier(),
                (EdgeProvider<Location, DefaultEdge>) new EdgeSupplier());

        importer.importGraph(this, input);
    }

    public void exportMap(File output) throws ExportException {
        JSONExporter<Location, DefaultEdge> exporter = new JSONExporter<Location, DefaultEdge>(
                new VertexDetailSupplier(), new VertexDetailSupplier(), new EdgeDetailSupplier(),
                new EdgeDetailSupplier());

        exporter.exportGraph(this, output);
        
    }

    public void exportMap(OutputStream output) throws ExportException {
        JSONExporter<Location, DefaultEdge> exporter = new JSONExporter<Location, DefaultEdge>(
                new VertexDetailSupplier(), new VertexDetailSupplier(), new EdgeDetailSupplier(),
                new EdgeDetailSupplier());

        exporter.exportGraph(this, output);
    }

    public void exportMap(Writer output) throws ExportException {
        JSONExporter<Location, DefaultEdge> exporter = new JSONExporter<Location, DefaultEdge>(
                new VertexDetailSupplier(), new VertexDetailSupplier(), new EdgeDetailSupplier(),
                new EdgeDetailSupplier());

        exporter.exportGraph(this, output);
    }

    public void Register(String id, Location location) {
        locationMap.put(id, location);
    }

    public Location getLocation(String id){
        return locationMap.get(id);
    }

    public ImmutableCollection<Location> getLocations(){
        return new ImmutableCollection<Location>(locationMap.values());
    }

    public ImmutableCollection<Location> getPath(String sourceName, String destinationName){
        Location source = getLocation(sourceName);
        Location destination = getLocation(destinationName);

        DijkstraShortestPath<Location,DefaultEdge> pathCalculator = new DijkstraShortestPath<>(this);
        GraphPath<Location, DefaultEdge> path = pathCalculator.getPath(source, destination);

        return new ImmutableCollection<Location>(path.getVertexList());
    }


    private class VertexDetailSupplier implements ComponentNameProvider<Location>, ComponentAttributeProvider<Location> {

        @Override
        public java.util.Map<String, Attribute> getComponentAttributes(Location component) {
            return component.getAttributes();
        }

        @Override
        public String getName(Location component) {
            return component.getName();
        }
    }

    private static class EdgeDetailSupplier
            implements ComponentNameProvider<DefaultEdge>, ComponentAttributeProvider<DefaultEdge> {

        int index = 0;

        @Override
        public java.util.Map<String, Attribute> getComponentAttributes(DefaultEdge component) {
            return new java.util.HashMap<String, Attribute>();
        }

        @Override
        public String getName(DefaultEdge component) {
            return Integer.toString(index++);
        }

    }

    public class EdgeSupplier implements EdgeProvider<Location, DefaultEdge>{

        @Override
        public DefaultEdge buildEdge(Location from, Location to, String label, Map<String, Attribute> attributes) {
            return SimulationMap.this.addEdge(from,to);
        }

    }

    public class VertexSupplier implements VertexProvider<Location> {

        @Override        
        public Location buildVertex(String id, java.util.Map<String, Attribute> attributes) {
            Location vertex = null;

            try {
                Class<?> clazz = Class.forName("Simulator.Model.Map." + attributes.get("Type").getValue());
                Object objOfType = clazz.getConstructor(SimulationMap.class ,String.class, java.util.Map.class).newInstance(SimulationMap.this, id, attributes);
                if(objOfType instanceof Location)
                    vertex = (Location) objOfType;
                if(attributes.containsKey("startingLocation") 
                    && attributes.get("startingLocation").getValue().equals("true"))
                    setStartingLocation(vertex);
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                    | InvocationTargetException | NoSuchMethodException | SecurityException 
                    | ClassNotFoundException e) {
                e.printStackTrace();
                    }

            return vertex;
        }
    }
}