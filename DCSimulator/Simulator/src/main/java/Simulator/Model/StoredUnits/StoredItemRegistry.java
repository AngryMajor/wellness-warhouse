package Simulator.Model.StoredUnits;

import java.util.*;

import org.springframework.stereotype.Service;

import CustomDataStructures.ImmutableCollection;
import Simulator.Services.StockRegistry;

@Service
public class StoredItemRegistry implements StockRegistry {

    List<StoredUnit> storedUnits = new ArrayList<>();

    @Override
    public void addStock(StoredUnit unit) {
        storedUnits.add(unit);
    }

    @Override
    public ImmutableCollection<StoredUnit> getStoredItems() {
        return new ImmutableCollection<>(storedUnits);
    }



}