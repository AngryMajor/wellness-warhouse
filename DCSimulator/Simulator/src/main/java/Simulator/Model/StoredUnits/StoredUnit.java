package Simulator.Model.StoredUnits;

import com.fasterxml.jackson.annotation.JsonView;

import Simulator.JacksonSettings.View;
import Simulator.Model.Map.Location;
import wellnessWarehouse.common.StockUnit;

public class StoredUnit{

	@JsonView(View.listing.class)
	private String name;
	@JsonView(View.listing.class)
	private Location currentPosition;

	public StoredUnit(StockUnit unit, Location currentPosition) {
		this.name = unit.getName();
		this.currentPosition = currentPosition;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Location getCurrentPosition() {
		return currentPosition;
	}

	public void setCurrentPosition(Location currentPosition) {
		this.currentPosition = currentPosition;
	}

}