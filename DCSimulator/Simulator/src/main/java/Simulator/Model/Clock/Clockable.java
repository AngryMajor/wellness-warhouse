package Simulator.Model.Clock;

public interface Clockable{

    void ready(Tickable tickable);
    void unready(Tickable tickable);
    boolean isReady(Tickable tickable);
    public void register(Tickable tickable);
    public int getTickNumber();

}