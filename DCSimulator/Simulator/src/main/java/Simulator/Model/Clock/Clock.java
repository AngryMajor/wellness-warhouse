package Simulator.Model.Clock;

import java.io.IOException;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import okhttp3.*;

import Simulator.CustomExceptions.DuplicateClockException;
import Simulator.CustomExceptions.UninstantiatedClockException;
import Simulator.Services.ClockService;

@Service
public class Clock implements ClockService, Clockable{

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");


    private Set<Tickable> readyTickables = new HashSet<Tickable>();
    private Set<Tickable> unreadyTickables = new HashSet<Tickable>();
    private static Clock instance;

    private boolean started;
    private int maxTimeBetweenTicks = 10;
    private Timer timer = new Timer();
    private ClockTickTimerAction clockTickTimerAction;

    private List<String> subscribers = new LinkedList<>();
    private int tickNumber = 0;

    @Autowired
    protected ClockEventSystem eventSystem;

    public Clock(){
        if(instance != null)
            throw new DuplicateClockException();

        instance = this;
        started = false;
    }

    public static Clock getInstance(){
        if(instance == null)
            throw new UninstantiatedClockException();
            
        return instance;
    }

    public static void clear(){
        instance = null;
    }

    public void register(Tickable item){
        unreadyTickables.add(item);
    }

    public void addSubscriber(String address){
        subscribers.add(address);
    }

    private void informSubscribers(){
        for(String subscriber : subscribers){
            if(subscriber != null){
                eventSystem.sendTick(subscriber, tickNumber);
            }
        }
    }

    private void runTimer(){
        if(clockTickTimerAction != null)
            clockTickTimerAction.cancel();
        clockTickTimerAction = new ClockTickTimerAction(this);
        timer.schedule(clockTickTimerAction, maxTimeBetweenTicks);
    }

    public void start(){
        if(started == false)
            runTimer();
        started = true;
    }

    public void stop(){
        started = false;
    }

    public boolean isStarted(){
        return started;
    }

    private void attemptTick(){
        if(shouldTick())
            tick();
    }

    private boolean shouldTick(){
        return (getPercentOfReadyTickables() >= 100);
    }

    public void tick(){
        informSubscribers();
        tickNumber++;
        Set<Tickable> toTickList = new HashSet<>();
        toTickList.addAll(readyTickables);

        for(Tickable item : toTickList)
            item.tick();
        if(isStarted())
            runTimer();
    }

    @Override
    public void ready(Tickable tickable) {
        if(unreadyTickables.contains(tickable)){
            unreadyTickables.remove(tickable);
            readyTickables.add(tickable);
        }
        
        if(isStarted()) attemptTick();
    }

    @Override
    public void unready(Tickable tickable) {
        if(readyTickables.contains(tickable)){
            readyTickables.remove(tickable);
            unreadyTickables.add(tickable);
        }
    }

    @Override
    public boolean isReady(Tickable tickable) {
        return readyTickables.contains(tickable);
    }

    public void setMaxTimeBetweenTicks(int value){
        maxTimeBetweenTicks = value;
    }

    public int getTickNumber(){
        return tickNumber;
    }

    public int getPercentOfReadyTickables(){
        if(getNumberRegisteredTickables()== 0)
            return 0;
        else
            return (int) ((double)readyTickables.size()/(double)getNumberRegisteredTickables() *100);
    }

    public int getNumberRegisteredTickables(){
        return readyTickables.size() + unreadyTickables.size();
    }

    private static class ClockTickTimerAction extends TimerTask{

        Clock clock;

        public ClockTickTimerAction(Clock clock){
            this.clock = clock;
        }

        @Override
        public void run() {
            clock.tick();
        }

    }

}