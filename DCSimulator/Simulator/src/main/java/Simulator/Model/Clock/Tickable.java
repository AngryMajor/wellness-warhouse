package Simulator.Model.Clock;

public interface Tickable{
    public void tick();
}