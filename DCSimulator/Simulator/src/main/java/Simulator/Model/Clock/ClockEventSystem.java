package Simulator.Model.Clock;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ClockEventSystem{

    private final RestTemplate restTemplate;

    public ClockEventSystem(RestTemplateBuilder builder){
        restTemplate = builder.build();
    }

    public void sendTick(String targetUrl, int tickNumber){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String content = "{\"time\":"+tickNumber+"}";

        HttpEntity<String> request = new HttpEntity<>(content, headers);
        restTemplate.put(targetUrl, request);
    }

}