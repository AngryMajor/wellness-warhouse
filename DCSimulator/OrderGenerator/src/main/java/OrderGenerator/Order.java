package OrderGenerator;

import java.util.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import wellnessWarehouse.common.StockUnit;

public class Order extends ArrayList<StockUnit>{
    private static final long serialVersionUID = 1L;

    public String toJson(){
        String json = "[";

        Iterator<StockUnit> units = this.iterator();

        StockUnit unit;
        ObjectMapper mapper = new ObjectMapper();
        
        try{
            if(units.hasNext()){
                unit = units.next();
                json += mapper.writeValueAsString(unit);;
            }

            while(units.hasNext()){
                unit = units.next();
                json += "," + mapper.writeValueAsString(unit);;
            }
        }catch(JsonProcessingException e){
            e.printStackTrace();
        }

        return json + "]";
    }

}
