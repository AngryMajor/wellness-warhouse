package OrderGenerator;

import java.util.Random;

import StockGenerator.StockUnitSource;

public class OrderGenerator {

    StockUnitSource stockSource;

    public OrderGenerator(StockUnitSource stockSource){
        this.stockSource = stockSource;
    }

    public Order generateOrder(){
        return generateOrder(.5f);
    }

    public Order generateOrder(float ProbabilityOfAdditionalItem){
        return generateOrder(ProbabilityOfAdditionalItem,10);
    }

    /**
     * 
     * @param ProbabilityOfAdditionalItem: value between 0..1 have adding an item to the order, this is tested repeatedly until fail
     * @return
     */
    public Order generateOrder(float ProbabilityOfAdditionalItem, int maxSize){
        Order order = new Order();

        double anotherItem = 0;
        while(order.size() < maxSize && anotherItem <= ProbabilityOfAdditionalItem){
            int randItemIndex = new Random().nextInt(stockSource.numberOfStockItems());
            order.add(stockSource.getStockUnit(randItemIndex));
            anotherItem = new Random().nextDouble();
        }

        return order;
    }

}