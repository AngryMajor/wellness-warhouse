package Web;

import java.io.IOException;

import OrderGenerator.Order;
import okhttp3.*;

public class OrderClient{

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    protected OkHttpClient client = new OkHttpClient();
    protected String targetUrl;

    public OrderClient(String targetUrl){
        this.targetUrl = targetUrl;
    }

    public void sendOrder(Order order){

        String orderBody = "{\"order\":"+order.toJson()+"}";
        RequestBody body = RequestBody.create(orderBody,JSON);

        Request request = new Request.Builder()
            .url(targetUrl)
            .post(body)
            .build();

        try{
            Response response = client.newCall(request).execute();
            System.out.println(response);
        }catch(IOException e){
            e.printStackTrace();
        }
    }

}