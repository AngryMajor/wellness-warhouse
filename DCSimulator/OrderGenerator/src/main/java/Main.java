import java.io.BufferedReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.Random;

import OrderGenerator.Order;
import OrderGenerator.OrderGenerator;
import StockGenerator.StockCatalog;
import Web.OrderClient;

public class Main{
    private static final int MIN_WAIT = 1000;
    private static final int MAX_WAIT = 4000;
    private static final float ADDITIONAL_ITEM_PROB = 0.4f;
    private static final int MAX_ITEM_COUNT = 10;



    public static void main(String[] args) throws InterruptedException {
        Random rand = new Random();

        OrderClient client = new OrderClient(args[0]);
        StockCatalog catalog = new StockCatalog();
        Reader jsonReader = new StringReader("[{\"name\":\"name\"}]");
        catalog.loadCatalogFrom(jsonReader);
        OrderGenerator generator = new OrderGenerator(catalog);

        while(true){
            int timeBetweenOrders = rand.nextInt(MAX_WAIT - MIN_WAIT) + 1 + MIN_WAIT;
            Thread.sleep(timeBetweenOrders);
            Order order = generator.generateOrder(ADDITIONAL_ITEM_PROB,MAX_ITEM_COUNT);
            client.sendOrder(order);
        }
    }

}