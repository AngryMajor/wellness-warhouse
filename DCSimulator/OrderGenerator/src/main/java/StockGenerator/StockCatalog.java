package StockGenerator;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import wellnessWarehouse.common.StockUnit;

public class StockCatalog implements StockUnitSource {

    List<StockUnit> stockList = new ArrayList<StockUnit>();

    @Override
    public StockUnit getStockUnit(int index) {
        return stockList.get(index);
    }

    public void addStockUnit(StockUnit su){
        stockList.add(su);
    }

    public int numberOfStockItems(){
        return stockList.size();
    }

    public StockUnit getStockUnitProbabilistically(int randomNumber){
        return null;
    }

    public void loadCatalogFrom(Reader jsonReader) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            StockUnit[] stocks = mapper.readValue(jsonReader, StockUnit[].class);
            for(int i = 0; i < stocks.length; i++)
                stockList.add(stocks[i]);

        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}