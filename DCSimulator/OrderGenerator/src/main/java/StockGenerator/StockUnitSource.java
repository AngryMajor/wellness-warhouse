package StockGenerator;

import wellnessWarehouse.common.StockUnit;

public interface StockUnitSource{
    public StockUnit getStockUnit(int rank);
    public int numberOfStockItems();
}