import mams.HALConverter;
import java.util.List;

agent PickerAgent extends mams.PassiveMAMSAgent{
    module Console console;
    module System system;
    module HALConverter hal;
    module Functions F;
    module Prelude pre;
    module Debug debug;
    module Strings str;
    module ListConverter lc;

    types a{
        formula url(string,string);//(id, url)

        formula currentPosition(string);//(id)
        formula position(string);//(id)
        formula neighbor(string, string);//(source,dest)
        formula adjacent(string,string);
        formula aisle(string,string,string);//(id, startAlly, endAlly)
        formula ally(string,list,list);//(id, connecting Aisles, connecting Allies)
        formula path(string, list);//(taskID, list of nodes to destination)

        formula task(string, string, string);//(id, location, actoin)
        formula taskList(list);
        formula currentTask(string);
        formula currentAction(string);

        formula currentFatigue(double);
    }

    inference adjacent(string x, string y) :- neighbor(x,y) | neighbor(y,x);

    initial url("entityService","http://simulator:8080/pickers");
    initial url("clockService","http://simulator:8080/clock");
    initial url("map","http://simulator:8080/map");

    initial currentPosition("Depot");
    initial !Init(); 
    //initial !destination("aisle.1.a");

    /////////////////////////
    //Setup
    /////////////////////////

    rule +!Init(){
        +url("thisAgent","http://picker:9000/" + system.name());
        MAMSAgent::!init();
        MAMSAgent::!created("base");

        console.println("waiting");
        system.sleep(20000);

        !CreateEntityEntity();
        !SubscribeToClock();
        !itemResource("task","Task");
        !itemResource("task_list","TaskList");
        !UpdateKnolage();

        //!MoveInfinitly();

        debug.dumpBeliefs();
        debug.printEventQueue();
    }

    rule +!MoveInfinitly(){
        +task("MoveTo1.3","Aisle.1.3","null");
        !PerformTask("MoveTo1.3");
        +task("MoveTo2.3","Aisle.2.3","null");
        !PerformTask("MoveTo2.3");
        !MoveInfinitly();
    }

    rule +!CreateEntityEntity() : url("entityService",string entityUrl){
        try{
            !EntityParams(string params);
            MAMSAgent::!post(entityUrl,params,int code, string content);
            !handel(hal.toRawFunct("entityCreated",content));
            cartago.println("Entity Created: " + entityUrl);
        }
        recover{
            system.sleep(1000);
            !!CreateEntityEntity();
        }
    }

    rule +!EntityParams(string params){
        params = hal.toJsonString(m(name(system.name())));
    }

    rule +!handel(entityCreated(Location(string url))){
        +url("myEntity",url);
    }

    rule +!SubscribeToClock() : url("clockService", string clockUrl) & url("thisAgent",string myUrl) {
        try{
        PassiveMAMSAgent::!itemResource("tick","Tick");
        string params = hal.toJsonString(m(location(myUrl+"/tick")));
        MAMSAgent::!post(clockUrl+"/subscribers",params,int code, string content);
        }
        recover{
            system.sleep(1000);
            !SubscribeToClock();
        }
    }

    rule +$cartago.property(string _, name(string task_id)) : task_id ~= "" {
        system.sleep(100);
        !itemProperty("task","position",funct position_f);
        !itemProperty("task","action",funct action_f);

        !handel(task_id,position_f,action_f);
        !PerformTask(task_id);
    }

    rule +!handel(string task_id, position(string position), action(string action)){
        cartago.println("new task: " + task_id + "|" + position + "|" + action);
        +task(task_id,position,action);
    }

    rule +$cartago.property(string _, tasks(List l)) {
        list tasks = lc.AsListTerm(l);
        cartago.println(" " + tasks);
        -+taskList(list any);
        !handel(tasks);
        query(taskList(list listOTasks));
        cartago.println(" " + listOTasks);
    }

    rule +!handel(list task_list) : ~(pre.isEmpty(task_list)){
        funct task_f = pre.valueAsFunct(task_list,0);
        pre.remove(task_list,0);
        string name = F.valueAsString(task_f,0);
        string position = F.valueAsString(task_f,1);
        string action = F.valueAsString(task_f,2);
        query(taskList(list listOTasks));
        pre.add(listOTasks,name);
        !handel(name,position(position),aciton(action));
        !handel(task_list);
    }

    rule +!handel(list task_list){}

    rule -currentTask(string task_id) : task(task_id,string position, string action)
    & artifact("task",string _, cartago.ArtifactId taskM_id){
        -task(task_id,position,action);
        cartago.operation(taskM_id, setStringProperty("name",""));
        cartago.operation(taskM_id, setStringProperty("position",""));
        cartago.operation(taskM_id, setStringProperty("action",""));

    }

    ///////////////////////// 
    //onTick
    /////////////////////////
    
    rule +$cartago.property(string name, time(int t)) {
        !UpdateKnolage();
    }

    goal +!PerformTask(string task_name) : task(task_name, string location, string action) <: ~task(task_name, string l, string a){
        body {           
            console.println("started performing " + task_name);
            +currentTask(task_name);
            debug.dumpBeliefs();
            !FatigueLevel(0.9);
            console.println("moveing toward " + location);
            !CurrentPosition(location);
            !PerformingAction(action);
            console.println("finished performing " + task_name);
            -currentTask(task_name);
            -task(task_name,location,action);
        }
    }

    rule -!CurrentPosition(string destination) : path(string taskID, list l){
        console.println(destination + " reached");
        -path(taskID, l);
    }

    goal +!FatigueLevel(double targetValue) <: currentFatigue(double curr_fatigue) 
        & (curr_fatigue <= targetValue){
        body {
            !StartResting();
        }

        rule +!StartResting() : currentFatigue(double fatigue)
        & (fatigue > targetValue){
            cartago.println("resting");
            string json_params = hal.toJsonString(action(type("Rest")));
            !SendAction(json_params);
            +currentAction("Rest");
        }

        rule +!StartResting(){
            cartago.println("not resting in goal ");
        }

    }
    rule +!StartResting(){
        cartago.println("not resting");
    }


    goal +!CurrentPosition(string destination) <: currentPosition(destination){
        body{
            !Move();
        }

        rule -currentAction(string old_action){
            !Move();
        }

        rule +!Move() : currentPosition(string curr_position) & currentTask(string taskID) & path(taskID, list locations_on_path){
            string next_location = pre.headAsString(locations_on_path);
            pre.remove(locations_on_path,0);
            string json_params = hal.toJsonString(action(type("Move"),destination(next_location)));
            !SendAction(json_params);
            +currentAction("Move");
        }

        rule +!Move() : currentPosition(string curr_position) & url("map",string map_url){
            try{
            MAMSAgent::!get(map_url +"/paths/"+ curr_position +"/"+ destination, int code, string content);
            funct f = hal.toRawFunct("path",content);
            !handel(f);
            !Move();
            }
            recover{
                system.sleep(1000);
                !Move();
            }
        }

        rule +!handel(path(path(list locations_on_path), length(int length))) : currentPosition(string curr_position) & currentTask(string taskID){
            pre.remove(locations_on_path,0);
            +path(taskID,locations_on_path);
        }

    }

    rule +!Move(){}

    goal +!PerformingAction(string action){
        body{
            !Act();
        }

        rule -currentAction(string old_action){
            !CheckIfActionWasSuccessful();
            console.println("finished action: " + old_action);
        }

        rule +!Act(){
            string action_name = F.valueAsString(F.valueAsFunct(hal.toRawFunct("action",action),0),0);
            console.println("performing aciton: " + action_name);
            !SendAction(action);
            +currentAction(action_name);
        }

        rule +!CheckIfActionWasSuccessful(){
            done;
        }
    }

    rule +!SendAction(string json_params) : url("myEntity",string entity_url){
        try{
        MAMSAgent::!put(entity_url+"/action",json_params,int code, string content);
        }
        recover{
            system.sleep(1000);
            !SendAction(json_params);
        }
    }

    /////////////////////////
    //Knolage update
    /////////////////////////

    rule +!UpdateKnolage(){
        system.sleep(50);//this is to let simulation finish updateing
        !UpdateCurrentposition();
        !UpdateCurrentAction();
        !UpdateFatigue();
    }

    rule +!UpdateFatigue() : url("myEntity",string entityUrl){
        try{
            cartago.println("updating fatigue");
            MAMSAgent::!get(entityUrl + "/fatigue", int code, string content);
            funct f = hal.toRawFunct("fatigue",content);
            !handel(f);
        }
        recover{
            system.sleep(1000);
            !UpdateFatigue();
        }    
    }

    rule +!handel(fatigue(generalFatigue(double value))){
        -+currentFatigue(value);
        cartago.println("current fatigue is: " + value);
    }

    rule +!UpdateCurrentAction() : url("myEntity",string entityUrl){
        try{
        MAMSAgent::!get(entityUrl + "/action", int code, string content);
        funct f = hal.toRawFunct("action",content);
        cartago.println("current action returned: " + f);
        !handel(f);
        }
        recover{
            system.sleep(1000);
            !UpdateCurrentAction();
        }
    }

    rule +!handel(action(name("null"))) : currentAction(string actionName){
        -currentAction(actionName);
    }

    rule +!handel(action(name("null"))){
        +currentAction("null");
        -currentAction("null");
    }

    rule +!handel(action(name(string name))) : currentAction(name){
    }

//TODO: Figure out what to do if the entity is asigned an action but I didn't know about it
    rule +!handel(action(name(string entity_action))) : currentAction(string my_action){
        
    }

    rule +!UpdateCurrentposition() : url("myEntity",string entityUrl){
        try{
            MAMSAgent::!get(entityUrl + "/position", int code, string content);
            funct f = hal.toRawFunct("position",content);
            !handel(f);
            string positonName = F.valueAsString(F.valueAsFunct(f,0), 0);
            console.println("current location: " + positonName);
            -+currentPosition(positonName);
        }
        recover{
            system.sleep(1000);
            !UpdateCurrentposition();
        }
    }

    rule +!handel(position(name(string name),adjPoses(list neighbors)))  {
        //if( str.startsWith(name,"Aisle"))
            //console.println("in an aisle");
        //+position(name);
        //!PopulateNeighbors(name,neighbors);
    }

    /*rule +!buildAisle(string name) : ~aisle(name, string a, string b){

    }

    rule +!PopulateNeighbors(string source, list neighbors) {
        if(pre.size(neighbors) > 0){
            +neighbor(source, pre.headAsString(neighbors));
            pre.remove(neighbors, 0);
            !PopulateNeighbors(source, neighbors);
        }
    }*/
}
