import java.util.ArrayList;
import java.util.List;


public class TaskList {
    public List<Task> tasks;

    public List<Task> getTasks(){
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}