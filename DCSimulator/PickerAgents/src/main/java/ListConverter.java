import java.util.List;

import astra.core.Module;
import astra.core.Module.ACTION;

import astra.formula.Predicate;
import astra.term.Funct;
import astra.term.ListTerm;
import astra.term.Primitive;
import astra.term.Term;

public class ListConverter extends Module{

    @TERM
    public ListTerm AsListTerm(List tasks) {

        ListTerm list = new ListTerm();
        for (Object item : tasks) {
            Task task = (Task) item;
            list.add(new Funct("task", new Term[] { 
                Primitive.newPrimitive(task.name),
                Primitive.newPrimitive(task.position),
                Primitive.newPrimitive(task.action)
            }));
        }
        return list;
    }

}