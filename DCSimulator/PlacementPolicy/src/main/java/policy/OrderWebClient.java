package policy;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import okhttp3.*;

@Service
public class OrderWebClient implements WebClient {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    @Value("${web-client.targets}")
    protected String[] targets;
    @Value("${web-client.map-source}")
    protected String mapSource;
    protected OkHttpClient client = new OkHttpClient();

    public void sendOrder(OrderMessage order) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String orderBody = mapper.writeValueAsString(order); 
        RequestBody body = RequestBody.create(orderBody,JSON);

        for(String targetUrl : targets){
            System.out.println(targetUrl);
            Request request = new Request.Builder()
                .url(targetUrl)
                .post(body)
                .build();
        
            try{
                Response response = client.newCall(request).execute();
                System.out.println(response);
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }

    public String getMapJson(){
        Request request = new Request.Builder().url(mapSource).build();
        try{
            Response response = client.newCall(request).execute();
            return response.body().string();
        }catch(IOException e){
            e.printStackTrace();
            return null;
        }
    }

}