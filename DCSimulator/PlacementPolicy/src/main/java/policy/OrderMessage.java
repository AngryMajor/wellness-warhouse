package policy;

import wellnessWarehouse.common.StockUnit;

public class OrderMessage {
    StockUnit[] order;

    public StockUnit[] getOrder() {
        return order;
    }

    public void setOrder(StockUnit[] orderedUnits) {
        this.order = orderedUnits;
    }
}