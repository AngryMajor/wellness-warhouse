package policy;

import com.fasterxml.jackson.core.JsonProcessingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import policy.OrderMessage;
import policy.PlacementPolicy;
import policy.WebClient;
import wellnessWarehouse.common.StockUnit;

@RestController
@RequestMapping("Order")
public class OrderController {

    @Autowired
    PlacementPolicy placer;
    @Autowired
    WebClient webClient;

    @PostMapping("")
    public ResponseEntity<?> newOrder(@RequestBody OrderMessage orderMessage) {
        for (StockUnit su : orderMessage.getOrder()) {
            placer.place(su);
        }

        try {
            webClient.sendOrder(orderMessage);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    

}