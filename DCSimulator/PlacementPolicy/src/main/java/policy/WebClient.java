package policy;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface WebClient {
    public void sendOrder(OrderMessage sus)throws JsonProcessingException ;
    public String getMapJson();
}