package policy;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import policy.MapRepresentation.Position;

@Service
public class DcMap {

    @Autowired
    WebClient webClient;
    JsonNode fullMapJson;
    ArrayNode mapNodes;

    List<Position> allPositions = new ArrayList<Position>();

    @PostConstruct
    public void run() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            fullMapJson = mapper.readTree(webClient.getMapJson());
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mapNodes = (ArrayNode) fullMapJson.get("jgraphtGraph").get("nodes");

        for(JsonNode nodeJson : mapNodes){
            allPositions.add(new Position(nodeJson));
        }
    }



}