package policy;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import wellnessWarehouse.common.StockUnit;

@Service
public class RandomPlacementPolicy implements PlacementPolicy {

    Random rand = new Random();

    @Autowired
    DcMap dcMap;

    public void place(StockUnit su) {
        int randomIndex = rand.nextInt(dcMap.allPositions.size());
        su.setAproxamatePosition(dcMap.allPositions.get(randomIndex).getName());
        su.setCurrentPosition(dcMap.allPositions.get(randomIndex).getName());
    }

}