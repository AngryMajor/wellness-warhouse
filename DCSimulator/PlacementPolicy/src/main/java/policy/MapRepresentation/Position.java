package policy.MapRepresentation;

import com.fasterxml.jackson.databind.JsonNode;

public class Position{

    String name;

	public Position(JsonNode nodeJson) {
        name = nodeJson.get("id").asText();
	}

	public String getName() {
		return name;
	}

}