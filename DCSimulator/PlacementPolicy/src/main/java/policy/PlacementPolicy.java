package policy;

import wellnessWarehouse.common.StockUnit;

public interface PlacementPolicy{
    public void place(StockUnit su);
}