#!/bin/bash

mvn package -f DCManagmentSystem/AdvocateAgents
sudo docker build DCManagmentSystem/AdvocateAgents --tag manager-advocates

mvn package -f DCManagmentSystem/TaskManager
sudo docker build DCManagmentSystem/TaskManager --tag manager-tasks

mvn package -f DCSimulator/Simulator
sudo docker build DCSimulator/Simulator --tag simulator-map

mvn package -f DCSimulator/PickerAgents
sudo docker build DCSimulator/PickerAgents --tag picker-agent

