package manager.controllers;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.TypeResolutionContext.Basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import manager.OrderRepository;
import manager.Orders.OrderUnit;
import wellnessWarehouse.common.StockUnit;

@RestController
@RequestMapping("/Orders")
public class OrderController {

    @Autowired
    OrderRepository orderDB;

    @PostMapping("")
    public void addOrder(@RequestBody OrderMessage order) throws JsonMappingException, JsonProcessingException {
        System.out.println(order);
        System.out.println(order.getOrder()[0]);
        for(StockUnit su : order.getOrder()){
            OrderUnit ou = OrderUnit.buildFromStockUnit(su);
            orderDB.save(ou);
        }

        List<OrderUnit> units = orderDB.findByName("name");
        System.out.println(units);

    }

    @GetMapping("")
    public void getAllOrders(){}


    public static class OrderMessage{
        StockUnit[] order;

        public StockUnit[] getOrder() {
            return order;
        }

        public void setOrder(StockUnit[] orderedUnits) {
            this.order = orderedUnits;
        }
    }
}