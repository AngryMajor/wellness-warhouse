package manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class SetupRunner implements CommandLineRunner{

    @Autowired
    OrderRepository orderDB;

    @Override
    public void run(String... args) throws Exception {
        orderDB.deleteAll();
    }

}