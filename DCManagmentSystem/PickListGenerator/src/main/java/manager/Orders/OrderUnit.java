package manager.Orders;

import javax.validation.Valid;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import wellnessWarehouse.common.StockUnit;

@Document
public class OrderUnit extends StockUnit{

    static int nextId = 1;

    public static OrderUnit buildFromStockUnit(StockUnit su){
        OrderUnit returnValue = new OrderUnit();
        returnValue.setName(su.getName());
        returnValue.id = nextId++;
        return returnValue;
    }

    @Id
    int id;
    Order myOrder;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        String sup = super.toString();
        return sup + " id: " + id;
    }

}