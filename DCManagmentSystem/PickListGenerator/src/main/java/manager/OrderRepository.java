package manager;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import manager.Orders.OrderUnit;

@Repository
public interface OrderRepository extends MongoRepository<OrderUnit,Integer>{
    List<OrderUnit> findByName(String name);
}
