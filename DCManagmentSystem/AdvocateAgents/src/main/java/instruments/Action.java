package instruments;

import com.fasterxml.jackson.databind.JsonNode;

public class Action {
    public String name;
    public int tickCompletedOn;
    public JsonNode params;
    public int tickStartedOn;

    @Override
    public String toString() {
        return "name:"+name +" started on:"+tickStartedOn +" completed on:"+tickCompletedOn +" parameters:"+params;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTickCompletedOn() {
        return tickCompletedOn;
    }

    public void setTickCompletedOn(int tickCompletedOn) {
        this.tickCompletedOn = tickCompletedOn;
    }

    public JsonNode getParams() {
        return params;
    }

    public void setParams(JsonNode params) {
        this.params = params;
    }

    public int getTickStartedOn() {
        return tickStartedOn;
    }

    public void setTickStartedOn(int tickStartedOn) {
        this.tickStartedOn = tickStartedOn;
    }
}