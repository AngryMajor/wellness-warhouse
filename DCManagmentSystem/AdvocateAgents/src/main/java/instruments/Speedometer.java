package instruments;

import java.io.IOException;
import java.util.Iterator;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import cartago.OPERATION;

public class Speedometer extends Instrument {
    double currentMoveTime = 0;

    @Override
    public void update() {
        try {
            Iterator<Action> actionIterator = getActionHistoryUpdate().iterator();
            Action lastMove = null;

            while(actionIterator.hasNext() && (lastMove = actionIterator.next()).name.equals("Move")==false)
            if(lastMove != null && lastMove.name.equals("Move")){
                currentMoveTime = lastMove.tickCompletedOn - lastMove.tickStartedOn;
            }
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }       
    }


}