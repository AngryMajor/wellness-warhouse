package instruments;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cartago.OPERATION;
import mams.artifacts.BaseArtifact;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public abstract class Instrument extends BaseArtifact {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final String SIMULATOR_ADDRESS = "http://simulator:8080/";

    protected static OkHttpClient client;
    protected String myPickerName;
    ObjectMapper mapper = new ObjectMapper();
    int tickOfLastUpdate = 0;

    @OPERATION
    @Override
    public void init(String pickerName) {
        client = new OkHttpClient();
        myPickerName = pickerName;
    }

    @OPERATION
    public abstract void update();

    protected String sendPickerGetRequest(String urlExtension) {

        Request request = new Request.Builder().url(SIMULATOR_ADDRESS + "/pickers/" + myPickerName + "/" + urlExtension)
                .build();

        try {
            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected List<Action> getActionHistorySince(int startTick)
            throws JsonParseException, JsonMappingException, IOException {
        String actionHistory = sendPickerGetRequest("action/history");
        Action[] history = mapper.readValue(actionHistory,Action[].class);
        List<Action> historySince = new ArrayList<Action>();
        for(int i = 0; i < history.length && history[i].tickCompletedOn > startTick; i++)
            historySince.add(history[i]);
        return historySince;
    }

    protected List<Action> getActionHistoryUpdate()
        throws JsonParseException, JsonMappingException, IOException {
            List<Action> update = getActionHistorySince(tickOfLastUpdate);
            if(update.isEmpty() == false)
                tickOfLastUpdate = update.get(0).tickCompletedOn;
            return update;
    }

}