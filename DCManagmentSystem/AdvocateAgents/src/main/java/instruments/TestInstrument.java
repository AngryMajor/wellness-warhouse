package instruments;

import cartago.OPERATION;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import cartago.ARTIFACT_INFO;
import cartago.OUTPORT;

@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class TestInstrument extends Instrument {
    ObjectMapper mapper = new ObjectMapper();

    ArrayList<JsonNode> actionLog = new ArrayList<JsonNode>();

    @OPERATION
    @Override
    public void init(String pickerName) {
        super.init(pickerName);
    }

    @OPERATION
    public void test(String name) {
        System.out.println("hello " + name);
    }

    @Override
    public void update() {
        String actionHistory = sendPickerGetRequest("action/history");
        try {
            for(Action a : getActionHistoryUpdate()){
                System.out.println(a);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(actionLog.isEmpty() == false)
            System.out.println(actionLog.get(actionLog.size()-1));
    }

}